$(function() {
  $("#loginForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    $.ajax({
      type: "POST",
      url: "/api/login",
      data: $("#loginForm").serialize(), // serializes the form's elements.
      success: function(data)
      {
         console.log(data); // show response from the php script.
      }
    });

    });
});
