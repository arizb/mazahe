$(function() {
  $("#registerForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    $.ajax({
      type: "POST",
      url: "/api/register",
      data: $("#registerForm").serialize(), // serializes the form's elements.
      success: function(data)
      {
         console.log(data); // show response from the php script.
      }
    });

    });
});
