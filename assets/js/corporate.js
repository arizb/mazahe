$(function() {
  $("#quoteForm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    $.ajax({
      type: "POST",
      url: "/api/quote",
      data: $("#quoteForm").serialize(), // serializes the form's elements.
      success: function(data)
      {
         console.log(data); // show response from the php script.
      }
    });

    });
});
