var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifyCss = require('gulp-clean-css');

var defaults = ['styles', 'vendors', 'scripts' ,'watch'];

gulp.task('default', defaults);

gulp.task('vendors', function() {
    gulp.src('bower_components/bootstrap/dist/css/*.css')
        .pipe(concat('vendors.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('web/css/vendors/'))
        .pipe(gulp.dest('public_html/css/vendors/'))
});

gulp.task('styles', function() {
    gulp.src('assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('web/css/'))
        .pipe(gulp.dest('public_html/css/'))
});

gulp.task('scripts', function() {
    gulp.src('assets/js/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('web/js/'))
        .pipe(gulp.dest('public_html/js/'))
});

gulp.task('watch', function() {
    gulp.watch('assets/js/*.js', ['scripts']);
    gulp.watch('assets/sass/*.scss', ['styles']);
});
