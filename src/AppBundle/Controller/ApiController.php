<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Accounts;
use AppBundle\Entity\Inquiries;
use AppBundle\Entity\Quote;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ApiController extends Controller
{
    /**
     * @Route("/api/register", name="register API")
     */
    public function registerAction(Request $request, EntityManagerInterface $em)
    {
      $arrRequest = $request->request->all();

      $accounts = new Accounts();
      $accounts->setName($arrRequest['name']);
      $accounts->setEmail($arrRequest['email']);
      $accounts->setPhone($arrRequest['phone']);
      $accounts->setPassword($arrRequest['password']);

      try {
        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($accounts);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();
        return $this->json(array('data' => 'Saved new account with id '.$accounts->getId()));

       } catch (UniqueConstraintViolationException $e) {
        return $this->json(array('data' => $e->getMessage()));
       }

    }

    /**
     * @Route("/api/login", name="login API")
     */
    public function loginAction(Request $request, EntityManagerInterface $em)
    {
      $arrRequest = $request->request->all();
      $repository = $this->getDoctrine()->getRepository(Accounts::class);

      $products = $repository->findBy(
        array('email' => $arrRequest['email'], 'password' => $arrRequest['password'])
      );
      if (!$products) {
        return $this->json(array('data' => 'No matched account'));
      }

      $encoders = array(new XmlEncoder(), new JsonEncoder());
      $normalizers = array(new ObjectNormalizer());
      $serializer = new Serializer($normalizers, $encoders);
      $products = $serializer->serialize($products, 'json');
      return $this->json($products);
    }

    /**
     * @Route("/api/contact", name="contact API")
     */
    public function contactAction(Request $request, EntityManagerInterface $em)
    {
        $arrRequest = $request->request->all();

        $inquiries = new Inquiries();
        $inquiries->setName($arrRequest['name']);
        $inquiries->setEmail($arrRequest['email']);
        $inquiries->setPhone($arrRequest['phone']);
        $inquiries->setMessage($arrRequest['message']);

        try {
            $em->persist($inquiries);
            $em->flush();
            return $this->json(array('data' => 'Saved/sent new message with id '.$inquiries->getId()));

        } catch (UniqueConstraintViolationException $e) {
            return $this->json(array('data' => $e->getMessage()));
        }

    }

    /**
     * @Route("/api/quote", name="quote API")
     */
    public function quoteAction(Request $request, EntityManagerInterface $em)
    {
        $arrRequest = $request->request->all();

        $quote = new Quote();
        $quote->setName($arrRequest['name']);
        $quote->setEmail($arrRequest['email']);
        $quote->setPhone($arrRequest['phone']);
        $quote->setCompany($arrRequest['company']);
        $quote->setLocation($arrRequest['location']);
        $quote->setMessage($arrRequest['message']);

        try {
            $em->persist($quote);
            $em->flush();
            return $this->json(array('data' => 'Saved/sent new request with id '.$quote->getId()));

        } catch (UniqueConstraintViolationException $e) {
            return $this->json(array('data' => $e->getMessage()));
        }

    }
}
